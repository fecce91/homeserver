const path              = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const extractLESS = new ExtractTextPlugin(path.join(__dirname, '/../public/build/bundle.css'));

module.exports = {
	context: path.join(__dirname, 'src/js'), 				// исходная директория с исходниками
	entry:   './index', 									// исходный файл для сборки
	output:  {
		path:     path.join(__dirname, '/../public/build'),	// выходная директория
		filename: 'bundle.js'								// выходное название файла
	},
	resolve: {
		modules:          ['node_modules', 'bower_components'],
		descriptionFiles: ['package.json', 'bower.json']
	},
	plugins: [
		extractLESS
	],
	devtool: "source-map",
	module:  {
		rules: [
			{
				test: /\.less$/i,
				use:  extractLESS.extract(['css-loader', 'less-loader'])
			}
		]
	},
};