<?php

use HomeServer\App;
use HomeServer\Config;
use HomeServer\Validation;
use Symfony\Component\Cache\Adapter\ChainAdapter;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

// debug tool
function pre(...$arguments)
{
	call_user_func_array('dump', $arguments);
}

// debug tool
function dd(...$arguments)
{
	call_user_func_array('pre', $arguments);
	die(0);
}

/**
 * Shortcut for getting variables from env
 *
 * @param string $key
 * @param null   $default
 *
 * @return string
 */
function env(string $key, $default = null)
{
	$value = getenv($key);
	if ($value === false) {
		return $default;
	}

	return $value;
}

/**
 * Shortcut for getting variables from config
 *
 * @param string $key
 * @param mixed  $default
 *
 * @return mixed
 */
function config(string $key, $default = null)
{
	return Config::instance()->get($key, $default);
}

/**
 * Shortcut for generating url for route with given parameters
 *
 * @param string $name
 * @param array  $parameters
 * @param bool   $fullPath
 *
 * @return string
 */
function route(string $name, array $parameters = [], bool $fullPath = false) : string
{
	/* @todo Переделать на какой-нить отдельный маппинг в конфигах */
	$baseDomain = env('BASE_DOMAIN');

	if ($name === 'oauth_google_auth') {
		$name       = 'localhost_' . $name;
		$baseDomain = 'localhost';
	}

	$parameters = array_merge(
		$parameters,
		[
			'domain' => $baseDomain
		]
	);

	return (new UrlGenerator(App::instance()->getRoutes(), App::instance()->getRequestContext()))->generate(
		$name,
		$parameters,
		$fullPath === true ? UrlGeneratorInterface::ABSOLUTE_URL : UrlGeneratorInterface::ABSOLUTE_PATH
	);
}

/**
 * Shortcut for create RedirectResponse Object
 *
 * @param string $url Target URL
 *
 * @return RedirectResponse
 */
function redirect(string $url) : RedirectResponse
{
	return new RedirectResponse($url);
}

/**
 * Perform quick validation
 *
 * @param mixed  $obj
 * @param string $rule
 *
 * @return bool
 */
function validate($obj, string $rule) : bool
{
	return Validation::instance()->validate($obj, $rule);
}

/**
 * Check if method in class is callable
 *
 * @param object $object
 * @param string $methodName
 *
 * @return bool
 */
function isCallable(object $object, string $methodName) : bool
{
	return is_callable([$object, $methodName]);
}

/**
 * Shortcut for creating common cookie
 *
 * @param string $key
 * @param string $value
 * @param int    $expireAt
 * @param bool   $httpOnly
 *
 * @return Cookie
 */
function cookie(string $key, string $value, ?int $expireAt = null, bool $httpOnly = false) : Cookie
{
	if (is_null($expireAt)) {
		$expireAt = config('app.cookie.lifetime', 3600);
	}

	return new Cookie(
		$key,
		$value,
		time() + $expireAt,
		'/',
		'.' . trim(route('root', [], false), '/'),
		boolval(env('APP_HTTPS_ONLY')),
		$httpOnly,
		false,
		Cookie::SAMESITE_STRICT
	);
}

/**
 * Shortcut for App::cache()
 *
 * @see App::cache()
 *
 * @return ChainAdapter
 */
function cache() : ChainAdapter
{
	return App::cache();
}
