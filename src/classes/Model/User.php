<?php

namespace HomeServer\Model;

use Defuse\Crypto\Crypto;
use Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException;
use Defuse\Crypto\KeyProtectedByPassword;
use HomeServer\Http\Controller\OAuth\GoogleOAuthUserAdapter;
use HomeServer\OAuth\GoogleOAuthAccessTokenData;
use HomeServer\Traits\LoggableTrait;
use Symfony\Component\HttpFoundation\Request;

/**
 * @property-read   int                        $id
 * @property        string                     $email
 * @property        string                     $firstName
 * @property        string                     $lastName
 * @property        int                        $createdAt
 * @property        int                        $updatedAt
 * @property        GoogleOAuthAccessTokenData $accessToken
 * @property        string                     $refreshToken
 * @property-read   string                     $salt
 * @property        string                     $imageUrl
 */
class User extends Model
{
	use LoggableTrait;

	protected $accessTokenObject;
	protected $decodedAccessToken;

	/**
	 * Flag for salt changes
	 *
	 * @var bool
	 */
	protected $saltChanged = false;

	/**
	 * @param $value
	 *
	 * @return \HomeServer\OAuth\GoogleOAuthAccessTokenData
	 */
	public function getAccessTokenAttribute($value) : GoogleOAuthAccessTokenData
	{
		if (!$this->decodedAccessToken) {
			$this->decodedAccessToken = json_decode($value, true);
		}

		if (!$this->accessTokenObject) {
			$this->accessTokenObject = GoogleOAuthAccessTokenData::createFromArray($this->decodedAccessToken ?? []);
		}

		return $this->accessTokenObject;
	}

	/**
	 * @param GoogleOAuthAccessTokenData $accessToken
	 */
	public function setAccessTokenAttribute(GoogleOAuthAccessTokenData $accessToken)
	{
		$this->accessTokenObject  = $accessToken;
		$this->decodedAccessToken = $accessToken->toArray();

		$salt = KeyProtectedByPassword::createRandomPasswordProtectedKey($accessToken->getAccessToken())->saveToAsciiSafeString();

		$this->attributes['accessToken'] = json_encode($this->decodedAccessToken, JSON_UNESCAPED_UNICODE);
		$this->attributes['salt']        = $salt;

		static::debug('New user salt', $this->id, $salt);

		$this->saltChanged = true;
	}

	/**
	 * Need to update sn and sk keys?
	 *
	 * @return bool
	 */
	public function needToUpdateCryptoKeys() : bool
	{
		return $this->saltChanged;
	}

	/**
	 * @return string
	 */
	public function generateSnKey() : string
	{
		return Crypto::encryptWithPassword($this->email, env('APP_KEY'));
	}

	/**
	 * @return string
	 */
	public function generateSkKey() : string
	{
		return Crypto::encryptWithPassword($this->email, $this->salt);
	}

	/**
	 * @param GoogleOAuthUserAdapter     $userInfo
	 * @param GoogleOAuthAccessTokenData $accessToken
	 * @param null|string                $refreshToken
	 *
	 * @return \HomeServer\Model\User
	 */
	public function updateFromGoogleInfo(GoogleOAuthUserAdapter $userInfo, GoogleOAuthAccessTokenData $accessToken, ?string $refreshToken = null) : User
	{
		static::debug('Update user from google info');

		$this->lastName    = $userInfo->getLastName();
		$this->firstName   = $userInfo->getFirstName();
		$this->imageUrl    = $userInfo->getImageUrl();
		$this->accessToken = $accessToken;

		if ($refreshToken) {
			$this->refreshToken = $refreshToken;
		}

		return $this;
	}

	/**
	 * Get User if it exists and crypto values are correct
	 *
	 * @param Request $request
	 *
	 * @return User|null
	 */
	public static function getUserFromRequest(Request $request) : ?User
	{
		$sn = $request->cookies->getAlnum('sn');
		$sk = $request->cookies->getAlnum('sk');

		if (!$sn || !$sk) {
			static::debug('SN or SK empty');

			return null;
		}

		static::debug('sn', $sn);
		static::debug('sk', $sk);

		$user = static::builder()
			->where('email', Crypto::decryptWithPassword($sn, env('APP_KEY')))
			->first();
		/* @var $user User */

		if (!$user) {
			static::warning('User not found');

			return null;
		}

		try {
			if ($user->email !== Crypto::decryptWithPassword($sk, $user->salt)) {
				static::warning('User email differ from decoded');

				return null;
			}
		} catch (WrongKeyOrModifiedCiphertextException $e) {
			static::warning('Wrong auth key', $sk, $user->salt, $e->getMessage());

			return null;
		}

		return $user;
	}
}
