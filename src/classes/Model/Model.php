<?php

namespace HomeServer\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as BaseModel;

abstract class Model extends BaseModel
{
	const CREATED_AT = 'createdAt';
	const UPDATED_AT = 'updatedAt';

	public static function builder() : Builder
	{
		return (new static())->newQuery();
	}
}
