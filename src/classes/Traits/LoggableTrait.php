<?php

namespace HomeServer\Traits;

use Monolog\Logger;

trait LoggableTrait
{
	/**
	 * @var \HomeServer\Log
	 */
	protected static $logger;

	/**
	 * Return name for logger
	 *
	 * @return string
	 */
	protected static function getLoggerName() : string
	{
		return static::class;
	}

	private static function log(int $level, string $message, array $args) : void
	{
		if (!static::$logger) {
			static::$logger = new \HomeServer\Log(static::getLoggerName());
		}

		static::$logger->log($level, $message, $args);
	}

	protected static function debug(string $message, ...$args) : void
	{
		static::log(Logger::DEBUG, $message, $args);
	}

	protected static function info(string $message, ...$args) : void
	{
		static::log(Logger::INFO, $message, $args);
	}

	protected static function notice(string $message, ...$args) : void
	{
		static::log(Logger::NOTICE, $message, $args);
	}

	protected static function warning(string $message, ...$args) : void
	{
		static::log(Logger::WARNING, $message, $args);
	}

	protected static function error(string $message, ...$args) : void
	{
		static::log(Logger::ERROR, $message, $args);
	}

	protected static function critical(string $message, ...$args) : void
	{
		static::log(Logger::CRITICAL, $message, $args);
	}

	protected static function alert(string $message, ...$args) : void
	{
		static::log(Logger::ALERT, $message, $args);
	}

	protected static function emergency(string $message, ...$args) : void
	{
		static::log(Logger::EMERGENCY, $message, $args);
	}
}
