<?php

namespace HomeServer\Traits;

/**
 * Class SingletonTrait
 * @package Traits
 */
trait SingletonTrait
{

	/**
	 * @var static
	 */
	protected static $instance;

	protected function __construct()
	{

	}

	/**
	 * @return static
	 */
	public static function instance()
	{
		if (!static::$instance) {
			static::$instance = new static();
		}

		return static::$instance;
	}
}
