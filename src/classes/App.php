<?php

namespace HomeServer;

use Dotenv\Dotenv;
use HomeServer\Exception\CoreException;
use HomeServer\Exception\NotFoundException;
use HomeServer\Http\Adapter\RouteAdapter;
use HomeServer\Http\Exception\HttpException404;
use HomeServer\Http\Middleware\AbstractMiddleware;
use HomeServer\Http\Response\EmptyResponse;
use HomeServer\Traits\LoggableTrait;
use HomeServer\Traits\SingletonTrait;
use HomeServer\Http\Controller\AbstractHttpController;
use Illuminate;
use Symfony\Component\Cache\Adapter\ApcuAdapter;
use Symfony\Component\Cache\Adapter\ChainAdapter;
use Symfony\Component\Cache\Adapter\PhpFilesAdapter;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Whoops\Handler\PlainTextHandler;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;
use Illuminate\Database\Capsule\Manager as Capsule;

class App
{
	use SingletonTrait;
	use LoggableTrait;

	/**
	 * @var RouteCollection
	 */
	protected $routes;

	/**
	 * @var Request
	 */
	protected $request;

	/**
	 * @var RequestContext
	 */
	protected $requestContext;

	/**
	 * @var RouteAdapter|null
	 */
	protected $routeAdapter;

	/**
	 * @var Session|null
	 */
	protected $session;

	/**
	 * @var ChainAdapter
	 */
	protected $cache;

	protected function __construct()
	{
		define('ROOT_NAMESPACE', __NAMESPACE__);

		$this->init();
	}

	protected function init()
	{
		$this
			->initErrors()
			->initConfig()
			->initCache()
			->initRoutes()
			->initDatabase();

		static::info('System initiated');

		$this->setRequest(Request::createFromGlobals());
	}

	public function runHttp()
	{
		static::info('Start HTTP request');

		$this->session = new Session();
		$this->session->start();

		$matcher = new UrlMatcher(
			static::instance()->getRoutes(),
			static::instance()->getRequestContext()
		);

		static::info('Trying to find route');

		try {
			$this->routeAdapter = new RouteAdapter($matcher->matchRequest(static::instance()->getRequest()));
		} catch (ResourceNotFoundException $e) {
			throw new HttpException404('', $e);
		}

		static::info('Route found', $this->needRouteAdapter()->getName());

		static::info('DB transaction start');
		/** @noinspection PhpUndefinedMethodInspection */
		Capsule::beginTransaction();

		$response = $this->callMiddleware();

		if (!$response) {
			$response = $this->callController();
		}

		/** @noinspection PhpUndefinedMethodInspection */
		Capsule::commit();
		static::info('DB transaction committed');

		static::info('Response', $response->getStatusCode(), get_class($response));
		if (get_class($response) === RedirectResponse::class) {
			/* @var $response RedirectResponse */
			static::info('Redirect url', $response->getTargetUrl());
		}

		$response->send();
		die(0);
	}

	/**
	 * @param Request $request
	 *
	 * @return App
	 */
	protected function setRequest(Request $request) : App
	{
		$this->request        = $request;
		$this->requestContext = (new RequestContext())
			->fromRequest($request)
			->setBaseUrl(env('BASE_URL', ''));

		return $this;
	}

	/**
	 * Get current request object
	 *
	 * @return Request
	 */
	public function getRequest() : Request
	{
		return $this->request;
	}

	/**
	 * @return RequestContext
	 */
	public function getRequestContext() : RequestContext
	{
		return $this->requestContext;
	}

	/**
	 * @return RouteCollection
	 */
	public function getRoutes() : RouteCollection
	{
		return $this->routes;
	}

	/**
	 * @param string $name
	 *
	 * @return Route|null
	 */
	public function getRoute(string $name) : ?Route
	{
		return $this->routes->get($name);
	}

	/**
	 * @param string $name
	 *
	 * @return Route
	 *
	 * @throws NotFoundException
	 */
	public function needRoute(string $name) : Route
	{
		$result = $this->routes->get($name);
		if (!$result) {
			throw new NotFoundException('Route ' . $name . ' not found');
		}

		return $result;
	}

	/**
	 * @return RouteAdapter|null
	 */
	public function getRouteAdapter() : ?RouteAdapter
	{
		return $this->routeAdapter ?? null;
	}

	/**
	 * @return RouteAdapter
	 *
	 * @throws CoreException
	 */
	public function needRouteAdapter() : RouteAdapter
	{
		if (!$this->routeAdapter) {
			throw new CoreException('Route Adapter is not init');
		}

		return $this->routeAdapter;
	}

	/**
	 * @return null|Session
	 */
	public function getSession() : ?Session
	{
		return $this->session;
	}

	/**
	 * @return Session
	 *
	 * @throws CoreException
	 */
	public function needSession() : Session
	{
		if (!$this->session) {
			throw new CoreException('Session is not init');
		}

		return $this->session;
	}

	/**
	 * @return ChainAdapter
	 */
	public function getCache() : ChainAdapter
	{
		return $this->cache;
	}

	/**
	 * @return ChainAdapter
	 */
	public static function cache() : ChainAdapter
	{
		return static::instance()->getCache();
	}

	/**
	 * Is it CLI request
	 *
	 * @return bool
	 */
	public static function isCli() : bool
	{
		return (php_sapi_name() === 'cli' OR defined('STDIN'));
	}

	/**
	 * @return App
	 */
	protected function initConfig() : App
	{
		// env must be init first, some config files can use env variables
		$env = new Dotenv(ROOT);
		$env->load();

		// loading config files
		Config::instance()->init();

		$env->required(config('env.required'));

		return $this;
	}

	/**
	 * @return App
	 */
	protected function initCache() : App
	{
		$apcCache = new ApcuAdapter(
			'',
			env('CACHE_DEFAULT_TIME', 3600)
		);

		$fileCache = new PhpFilesAdapter(
			'',
			env('CACHE_DEFAULT_TIME', 3600),
			ROOT . '/' . env('CACHE_DIR', null)
		);

		$this->cache = new ChainAdapter(
			[
				$apcCache,
				$fileCache
			]
		);

		return $this;
	}

	/**
	 * @return App
	 */
	protected function initErrors() : App
	{
		$whoops = new Run;

		if (static::isCli()) {
			$whoops->pushHandler(new PlainTextHandler());
		} else {
			$whoops->pushHandler(new PrettyPageHandler());
		}

		$whoops->register();

		return $this;
	}

	/**
	 * @return App
	 *
	 * @throws \HomeServer\Exception\CoreException
	 */
	protected function initRoutes() : App
	{
		$locator = new FileLocator([SRC]);
		$loader  = new YamlFileLoader($locator);

		$this->routes = $loader->load('routes.yml');

		return $this;
	}

	/**
	 * @return App
	 */
	protected function initDatabase() : App
	{
		$capsule = new Capsule();

		$capsule->addConnection(
			[
				'driver'    => 'mysql',
				'host'      => env('DB_HOST'),
				'database'  => env('DB_DATABASE'),
				'username'  => env('DB_USERNAME'),
				'password'  => env('DB_PASSWORD'),
				'charset'   => 'utf8',
				'collation' => 'utf8_unicode_ci',
				'prefix'    => '',
			]
		);

		$capsule->setAsGlobal();
		$capsule->bootEloquent();

		return $this;
	}

	/**
	 * Call controller by matched route
	 * @return Response
	 * @throws \Exception
	 */
	protected function callController() : Response
	{
		static::info(
			'Call controller',
			[
				'controller' => $this->needRouteAdapter()->getControllerClass(),
				'action'     => $this->needRouteAdapter()->getActionName()
			]
		);

		$controllerClass = $this->needRouteAdapter()->getControllerClass();

		try {
			$controller = new $controllerClass($this->getRequest());
			/* @var $controller AbstractHttpController */

			$response = $controller->{$this->needRouteAdapter()->getActionName()}();
			if (empty($response)) {
				$response = new EmptyResponse();
			}

			if ($controller->hasUser()) {
				$response = $controller->setCryptoCookiesToResponse($controller->getUser(), $response);
			}
		} catch (\Exception $e) {
			static::error('Error in controller', $controllerClass, $e, $e->getTrace());

			throw $e;
		}

		return $response;
	}

	/**
	 * @return null|Response
	 */
	protected function callMiddleware() : ?Response
	{
		static::info(
			'Run middleware',
			[
				'middleware' => $this->needRouteAdapter()->getMiddleware()
			]
		);

		$middlewareClasses = $this->needRouteAdapter()->getMiddleware();
		if (empty($middlewareClasses)) {
			return null;
		}

		foreach ($middlewareClasses as $middlewareClass) {
			$obj = new $middlewareClass($this->getRequest());
			/* @var $obj AbstractMiddleware */

			$response = $obj->handle($this->getRequest());
			if ($response) {
				return $response;
			}
		}

		return null;
	}
}
