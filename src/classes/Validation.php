<?php

namespace HomeServer;

use HomeServer\Exception\CoreException;
use HomeServer\Traits\SingletonTrait;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Validation as Validator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Validation
{
	use SingletonTrait;

	const URL = 'url';

	/**
	 * @var ValidatorInterface
	 */
	protected $validator;

	public function validate($object, $rule) : bool
	{
		$ruleName = 'validation_' . $rule;
		if (method_exists($this, $ruleName)) {
			return $this->{$ruleName}($object);
		}

		throw new CoreException('Validation rule ' . $rule . ' not exists');
	}

	/**
	 * Check if $object is a valid url
	 *
	 * @param mixed $object
	 *
	 * @return bool
	 */
	public function validation_url($object) : bool
	{
		$validator = $this->getValidator();

		$result = $validator->validate(
			$object,
			[
				new NotBlank(),
				new Url()
			]
		);

		return !$result->count();
	}

	/**
	 * @return ValidatorInterface
	 */
	protected function getValidator() : ValidatorInterface
	{
		if (!$this->validator) {
			$this->validator = Validator::createValidator();
		}

		return $this->validator;
	}
}
