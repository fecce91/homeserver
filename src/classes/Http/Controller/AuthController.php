<?php

namespace HomeServer\Http\Controller;

use HomeServer\App;
use HomeServer\Http\Response\EmptyResponse;
use HomeServer\Http\Response\HtmlResponse;
use HomeServer\Model\User;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends AbstractHttpController
{
	public function action_index()
	{
		return new HtmlResponse();
	}

	public function action_redirect() : Response
	{
		$backLink = App::instance()->getSession()->get('backLink');

		App::instance()->getSession()->remove('backLink');

		if ($backLink && validate($backLink, 'url')) {
			return redirect($backLink);
		} else {
			return redirect(route('auth_root'));
		}
	}

	public function action_sso() : Response
	{
		$response = new EmptyResponse();

		$response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);

		$sn = $this->getRequest()->cookies->getAlnum('sn');
		$sk = $this->getRequest()->cookies->getAlnum('sk');

		$userId = null;

		$cacheName = 'sso.' . md5($sn . $sk);

		$cacheItem = App::cache()->getItem($cacheName);
		if ($cacheItem->isHit()) {
			$userId = $cacheItem->get();
		} else {
			$user = User::getUserFromRequest($this->getRequest());
			if ($user) {
				$userId = $user->id;

				$cacheItem->set($userId);
				$cacheItem->expiresAfter(config('auth.cacheSSO', 60));

				App::cache()->save($cacheItem);
			}
		}

		if ($userId) {
			$response->setStatusCode(Response::HTTP_OK);
		} else {
			$response->setStatusCode(Response::HTTP_UNAUTHORIZED);
		}

		return $response;
	}
}
