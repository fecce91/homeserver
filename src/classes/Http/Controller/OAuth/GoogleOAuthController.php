<?php

namespace HomeServer\Http\Controller\OAuth;

use HomeServer\App;
use HomeServer\Http\Controller\AbstractHttpController;
use HomeServer\Model\User;
use HomeServer\OAuth\GoogleOAuthClient;

class GoogleOAuthController extends AbstractHttpController
{
	public function action_auth()
	{
		$client = GoogleOAuthClient::instance();

		if (!($code = $this->request->query->get('code'))) {
			throw new \InvalidArgumentException('Need code parameter in request');
		}

		$client->authenticate($code);

		$userInfo = $client->getUserInfo();

		$email = $userInfo->getEmail();

		$user = User::builder()
			->where('email', $email)
			->first();
		/* @var $user User */

		$user
			->updateFromGoogleInfo($userInfo, $client->getAccessToken(), $client->getRefreshToken())
			->save();

		$this->setUser($user);

		return redirect(route('auth_redirect_back'));
	}

	public function action_redirect()
	{
		$client = GoogleOAuthClient::instance();

		return redirect($client->getAuthUrl());
	}

	public function action_logout()
	{
		$client = GoogleOAuthClient::instance();
		$client->logout();

		App::instance()->getSession()->remove('userId');

		return redirect(route('auth_root'));
	}
}
