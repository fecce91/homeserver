<?php

namespace HomeServer\Http\Controller\OAuth;

class GoogleOAuthUserAdapter
{
	/**
	 * @var \Google_Service_Plus_Person
	 */
	protected $originalData;

	public function __construct(\Google_Service_Plus_Person $data)
	{
		$this->originalData = $data;
	}

	/**
	 * @return null|string
	 */
	public function getEmail() : ?string
	{
		$emails = $this->originalData->getEmails();
		/* @var $emails \Google_Service_Plus_PersonEmails[] */

		foreach ($emails as $email) {
			if ($email->getType() === 'account') {
				return $email->getValue();
			}
		}

		return null;
	}

	/**
	 * @return null|string
	 */
	public function getLastName() : ?string
	{
		$name = $this->originalData->getName();

		/* @var $name \Google_Service_Plus_PersonName */

		return $name->getFamilyName();
	}

	/**
	 * @return null|string
	 */
	public function getFirstName() : ?string
	{
		$name = $this->originalData->getName();

		/* @var $name \Google_Service_Plus_PersonName */

		return $name->getGivenName();
	}

	public function getImageUrl() : ?string
	{
		if ($image = $this->originalData->getImage()) {
			/* @var $image \Google_Service_Plus_PersonImage */

			return str_replace('?sz=50', '', $image->getUrl());
		}

		return null;
	}
}
