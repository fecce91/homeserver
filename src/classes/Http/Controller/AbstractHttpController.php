<?php

namespace HomeServer\Http\Controller;

use HomeServer\App;
use HomeServer\Exception\CoreException;
use HomeServer\Model\User;
use HomeServer\OAuth\GoogleOAuthClient;
use HomeServer\Traits\LoggableTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractHttpController
{
	use LoggableTrait;

	/**
	 * @var Request
	 */
	protected $request;

	/**
	 * @var User|null
	 */
	protected $user;

	public function __construct(Request $request)
	{
		$this->request = $request;

		if (App::instance()->getSession()->has('userId')) {
			$user = User::builder()->find(App::instance()->getSession()->get('userId'));
			/* @var $user User */

			$this->user = $user;

			if (!$this->getUser()) {
				throw new CoreException('Current user not found');
			}

			$client = GoogleOAuthClient::instance();
			$client->setAccessToken($this->getUser()->accessToken);
		}
	}

	/**
	 * @return Request
	 */
	public function getRequest() : Request
	{
		return $this->request;
	}

	/**
	 * @return User
	 */
	public function getUser() : ?User
	{
		return $this->user;
	}

	/**
	 * @return bool
	 */
	public function hasUser() : bool
	{
		return isset($this->user);
	}

	/**
	 * @param \HomeServer\Model\User $user
	 *
	 * @return AbstractHttpController|static
	 */
	public function setUser(User $user) : AbstractHttpController
	{
		$this->user = $user;

		return $this;
	}

	/**
	 * Set crypto cookies if needed
	 *
	 * @param User     $user
	 * @param Response $response
	 *
	 * @return Response
	 */
	public function setCryptoCookiesToResponse(User $user, Response $response) : Response
	{
		// checking if we need to update sk and sn
		if (!$user->needToUpdateCryptoKeys()) {
			return $response;
		}

		// generating and setting sn
		$sn = $user->generateSnKey();
		$response->headers->setCookie(cookie('sn', $sn, config('auth.lifetime'), true));

		// generating ans setting sk
		$sk = $user->generateSkKey();
		$response->headers->setCookie(cookie('sk', $sk, config('auth.lifetime'), true));

		static::info('Setting crypto cookies to response', $user->salt, $sn, $sk);

		return $response;
	}
}
