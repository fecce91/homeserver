<?php

namespace HomeServer\Http\Adapter;

use HomeServer\App;
use HomeServer\Exception\CoreException;
use HomeServer\Http\Middleware\AbstractMiddleware;
use Symfony\Component\Routing\Route;

class RouteAdapter
{
	/**
	 * @var string
	 */
	protected $controllerClass;

	/**
	 * @var string
	 */
	protected $actionName;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var Route
	 */
	protected $originalRoute;

	/**
	 * @var string[]
	 */
	protected $middleware = [];

	public function __construct(array $parameters)
	{
		if (empty($parameters['_controller'])) {
			throw new CoreException('Route does not have controller parameter');
		}

		$this->setControllerClass($parameters['_controller']);

		if (!class_exists($this->getControllerClass())) {
			throw new CoreException('Route use controller which does not exists');
		}

		if (empty($parameters['_action'])) {
			throw new CoreException('Route does not have action parameter');
		}

		$this->setActionName($parameters['_action']);

		if (!method_exists($this->getControllerClass(), $this->getActionName())) {
			throw new CoreException('Route use controller action which does not exists');
		}

		if (empty($parameters['_route'])) {
			throw new CoreException('Route does not have route (name) parameter');
		}

		$this->setName($parameters['_route']);

		$this->setOriginalRoute(App::instance()->needRoute($this->getName()));
	}

	/**
	 * @return string
	 */
	public function getControllerClass() : string
	{
		return $this->controllerClass;
	}

	/**
	 * @param string $controllerClass
	 *
	 * @return RouteAdapter
	 */
	protected function setControllerClass(string $controllerClass) : RouteAdapter
	{
		$this->controllerClass = ROOT_NAMESPACE . '\Http\Controller\\' . $controllerClass;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getActionName() : string
	{
		return $this->actionName;
	}

	/**
	 * @param string $actionName
	 *
	 * @return RouteAdapter
	 */
	protected function setActionName(string $actionName) : RouteAdapter
	{
		$this->actionName = 'action_' . $actionName;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getName() : string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 *
	 * @return RouteAdapter
	 */
	protected function setName(string $name) : RouteAdapter
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getMiddleware() : array
	{
		return $this->middleware;
	}

	/**
	 * @return Route
	 */
	public function getOriginalRoute() : Route
	{
		return $this->originalRoute;
	}

	/**
	 * @param Route $originalRoute
	 *
	 * @return RouteAdapter
	 *
	 * @throws \HomeServer\Exception\CoreException
	 */
	protected function setOriginalRoute(Route $originalRoute) : RouteAdapter
	{
		$this->originalRoute = $originalRoute;

		if ($originalRoute->hasOption('middleware')) {
			$middleware = (array)$originalRoute->getOption('middleware');

			foreach ($middleware as $name) {
				$className = config('middleware.aliases.' . $name);

				if (!$className || !class_exists($className) || !is_subclass_of($className, AbstractMiddleware::class)) {
					throw new CoreException('Middleware `' . $name . '` not exists');
				}

				$this->middleware[] = $className;
			}
		}

		return $this;
	}
}
