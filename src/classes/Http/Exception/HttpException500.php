<?php

namespace HomeServer\Http\Exception;

class HttpException500 extends AbstractHTTPException
{
	protected $httpCode = 500;
}
