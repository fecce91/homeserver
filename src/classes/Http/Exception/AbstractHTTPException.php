<?php

namespace HomeServer\Http\Exception;

abstract class AbstractHTTPException extends \Exception
{
	protected $httpCode = 500;
	protected $message  = "Internal Server Error";

	public function __construct(string $message = "", \Exception $previous = null)
	{
		if (empty($message)) {
			$message = $this->message;
		}

		parent::__construct($message, $this->httpCode, $previous);
	}
}
