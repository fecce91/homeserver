<?php

namespace HomeServer\Http\Exception;

class HttpException404 extends AbstractHTTPException
{
	protected $httpCode = 404;
	protected $message  = 'Page not found';
}
