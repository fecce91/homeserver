<?php

namespace HomeServer\Http\Response;

use HomeServer\App;
use Symfony\Component\HttpFoundation\Response;

class HtmlResponse extends Response
{
	/**
	 * @inheritdoc
	 */
	public function __construct($content = '', $status = 200, array $headers = [])
	{
		if (empty($content)) {
			$content = $this->getDefaultContent() ?? '';
		}

		parent::__construct($content, $status, $headers);
	}

	/**
	 * Get default content from html template file
	 *
	 * Warning!
	 * Template file will send as is.
	 *
	 * @return string|null
	 */
	protected function getDefaultContent() : ?string
	{
		$cacheItem = App::cache()->getItem('template.default');

		if (!$cacheItem->isHit()) {
			$filePath = config('app.template.default', false);
			if (!$filePath) {
				return null;
			}

			if (!file_exists($filePath)) {
				return null;
			}

			$content = file_get_contents($filePath);
			if (!$content) {
				return null;
			}

			$cacheItem->set($content);
			$cacheItem->expiresAfter(config('app.template.cache.lifetime', 60));

			App::cache()->save($cacheItem);

			return $content;
		}

		return $cacheItem->get();
	}
}