<?php

namespace HomeServer\Http\Middleware;

use HomeServer\App;
use HomeServer\Model\User;
use HomeServer\OAuth\GoogleOAuthClient;
use HomeServer\Traits\LoggableTrait;
use HomeServer\Validation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthMiddleware extends AbstractMiddleware
{
	use LoggableTrait;

	/**
	 * Handle an incoming request.
	 *
	 * @param Request $request Request data object
	 *
	 * @return null|Response
	 */
	public function handle(Request $request) : ?Response
	{
		static::info('Starting auth middleware');

		$backLink = null;
		if ($request->query->has('backLink')) {
			static::debug('Request has back link', $request->query->get('backLink'));

			App::instance()->getSession()->remove('backLink');

			$backLink = $request->query->get('backLink');
			if (validate($backLink, Validation::URL)) {
				static::info('Saving new back link', $backLink);

				App::instance()->getSession()->set('backLink', $backLink);
			}
		}

		$user = User::getUserFromRequest($request);
		if (!$user) {
			return redirect(route('oauth_google_redirect'));
		}

		try {
			$client = GoogleOAuthClient::instance();
			$client->setAccessToken($user->accessToken);

			if ($user->accessToken->isExpired()) {
				static::info('Access token is expired', $user->accessToken);

				$client->reAuthenticate($user->refreshToken);

				$user->updateFromGoogleInfo($client->getUserInfo(), $client->getAccessToken(), $client->getRefreshToken());
			} else {
				$cacheItem = App::cache()->getItem('auth.' . $user->id);
				if (!$cacheItem->isHit()) {
					$user->updateFromGoogleInfo($client->getUserInfo(), $client->getAccessToken(), $client->getRefreshToken());

					$cacheItem->set(true);
					$cacheItem->expiresAfter(config('google.cacheAuth', 3600));
					App::cache()->save($cacheItem);
				}
			}

			App::instance()->getSession()->set('userId', $user->id);

			static::info('Saving user');
			$user->save();

			static::info('Saving session variables');
			App::instance()->getSession()->save();
		} catch (\Google_Service_Exception $e) {
			static::warning('Error during getting user info from google', $e);

			return redirect(route('oauth_google_redirect'));
		}

		return null;
	}
}
