<?php

namespace HomeServer\Http\Middleware;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractMiddleware
{
	/**
	 * Handle an incoming request.
	 *
	 * @param Request $request Request data object
	 *
	 * @return null|Response
	 */
	abstract public function handle(Request $request) : ?Response;
}
