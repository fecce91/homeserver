<?php

namespace HomeServer\OAuth;

class GoogleOAuthAccessTokenData implements \Serializable, \JsonSerializable
{
	const TOKEN_INVALID_SHIFT = 60;

	/**
	 * @var string
	 */
	protected $accessToken;

	/**
	 * @var string
	 */
	protected $tokenType;

	/**
	 * @var int
	 */
	protected $expiresIn;

	/**
	 * @var string
	 */
	protected $idToken;

	/**
	 * @var int
	 */
	protected $createdAt;

	/**
	 * @var string
	 */
	protected $refreshToken;

	protected static $map = [
		'access_token' => 'accessToken',
		'token_type'   => 'tokenType',
		'expires_in'   => 'expiresIn',
		'id_token'     => 'idToken',
		'created'      => 'createdAt',
		'refreshToken' => 'refreshToken'
	];

	public function __construct(array $accessTokenData, string $refreshToken = null)
	{
		$this->fill($accessTokenData);
		$this->refreshToken = $refreshToken;
	}

	protected function fill(array $data) : GoogleOAuthAccessTokenData
	{
		foreach (static::$map as $key => $value) {
			$this->{$value} = $data[$key] ?? null;
		}

		return $this;
	}

	/**
	 * @return string
	 */
	public function getAccessToken() : string
	{
		return $this->accessToken;
	}

	/**
	 * @return string
	 */
	public function getRefreshToken() : string
	{
		return $this->refreshToken;
	}

	/**
	 * @return string
	 */
	public function getTokenType() : string
	{
		return $this->tokenType;
	}

	/**
	 * @return int
	 */
	public function getExpiresIn() : int
	{
		return $this->expiresIn;
	}

	/**
	 * @return string
	 */
	public function getIdToken() : string
	{
		return $this->idToken;
	}

	/**
	 * @return int
	 */
	public function getCreatedAt() : int
	{
		return $this->createdAt;
	}

	/**
	 * @param array $data
	 *
	 * @return GoogleOAuthAccessTokenData
	 */
	public static function createFromArray(array $data) : GoogleOAuthAccessTokenData
	{
		return new static($data);
	}

	/**
	 * @return bool
	 */
	public function isExpired() : bool
	{
		return (time() - $this->getCreatedAt() + static::TOKEN_INVALID_SHIFT) > $this->getExpiresIn();
	}

	/**
	 * @return array
	 */
	public function toArray() : array
	{
		$result = [];

		foreach (array_flip(static::$map) as $key => $value) {
			$result[$value] = $this->{$key} ?? null;
		}

		return $result;
	}

	/**
	 * @inheritdoc
	 */
	public function serialize()
	{
		return $this->jsonSerialize();
	}

	/**
	 * @inheritdoc
	 */
	public function unserialize($serialized)
	{
		$this->fill(json_decode($serialized));
	}

	/**
	 * @inheritdoc
	 */
	public function jsonSerialize()
	{
		return json_encode($this->toArray());
	}

	public function __toString() : string
	{
		return $this->serialize();
	}
}
