<?php

namespace HomeServer\OAuth;

use HomeServer\Http\Controller\OAuth\GoogleOAuthUserAdapter;
use HomeServer\Traits\LoggableTrait;
use HomeServer\Traits\SingletonTrait;

class GoogleOAuthClient
{
	use SingletonTrait;
	use LoggableTrait;

	/**
	 * @var \Google_Client
	 */
	protected $client;

	/**
	 * @var GoogleOAuthAccessTokenData
	 */
	protected $accessToken;

	protected function __construct()
	{
		$this->initClient();
	}

	protected function initClient() : GoogleOAuthClient
	{
		$client = new \Google_Client();

		$client->setClientId(env('GOOGLE_CLIENT_ID'));
		$client->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
		$client->addScope(
			[
				\Google_Service_Plus::USERINFO_EMAIL,
				\Google_Service_Plus::USERINFO_PROFILE,
			]
		);

		$client->setRedirectUri(route('oauth_google_auth', [], true));
		$client->setAccessType('offline');

		$this->client = $client;

		return $this;
	}

	/**
	 * Authenticate user with code
	 *
	 * @param string $code
	 *
	 * @return GoogleOAuthClient
	 */
	public function authenticate(string $code) : GoogleOAuthClient
	{
		$accessToken = $this->getClient()->fetchAccessTokenWithAuthCode($code);
		if (!$accessToken) {
			throw new \InvalidArgumentException('Google OAuth auth code invalid, could not get access token');
		}

		$this->accessToken = new GoogleOAuthAccessTokenData($accessToken);

		return $this;
	}

	/**
	 * Authenticate user with refresh token
	 *
	 * @param string $refreshToken
	 *
	 * @return GoogleOAuthClient
	 */
	public function reAuthenticate($refreshToken) : GoogleOAuthClient
	{
		static::info('ReAuthenticate', $refreshToken);

		$accessToken = $this->getClient()->fetchAccessTokenWithRefreshToken($refreshToken);
		if (!$accessToken) {
			throw new \InvalidArgumentException('Google OAuth auth code invalid, could not get access token');
		}

		$this->accessToken = new GoogleOAuthAccessTokenData($accessToken);

		return $this;
	}

	/**
	 * Get user info
	 *
	 * @return GoogleOAuthUserAdapter
	 */
	public function getUserInfo() : GoogleOAuthUserAdapter
	{
		$service = new \Google_Service_Plus($this->getClient());

		$adapter = new GoogleOAuthUserAdapter($service->people->get('me'));

		return $adapter;
	}

	/**
	 * @return GoogleOAuthAccessTokenData|null
	 */
	public function getAccessToken() : ?GoogleOAuthAccessTokenData
	{
		return $this->accessToken;
	}

	/**
	 * @return string|null
	 */
	public function getRefreshToken() : ?string
	{
		return $this->getClient()->getRefreshToken();
	}

	/**
	 * @return string
	 */
	public function getRedirectUrl() : string
	{
		return $this->getClient()->getRedirectUri();
	}

	/**
	 * @return string
	 */
	public function getAuthUrl() : string
	{
		return $this->getClient()->createAuthUrl();
	}

	/**
	 * @param GoogleOAuthAccessTokenData $accessToken
	 *
	 * @return GoogleOAuthClient
	 */
	public function setAccessToken(GoogleOAuthAccessTokenData $accessToken) : GoogleOAuthClient
	{
		$this->getClient()->setAccessToken($accessToken->toArray());

		$this->accessToken = $accessToken;

		return $this;
	}

	public function logout() : GoogleOAuthClient
	{
		$this->getClient()->revokeToken();

		return $this;
	}

	/**
	 * @return \Google_Client
	 */
	protected function getClient() : \Google_Client
	{
		return $this->client;
	}
}
