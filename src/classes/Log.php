<?php

namespace HomeServer;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\SyslogHandler;
use Monolog\Logger;

class Log
{
	/**
	 * @var Logger
	 */
	protected $logger;

	protected static $logId;

	public function __construct(string $name)
	{
		$handler = new SyslogHandler(config('log.name', 'server'));
		$handler->setFormatter(new LineFormatter());

		$this->logger = new Logger(static::getId() . '.' . $name, [$handler]);

		$this->log(Logger::DEBUG, 'Logger initiated');
	}

	public function log(int $level, string $message, array $context = [])
	{
		$this->logger->addRecord($level, $message, $context);
	}

	/**
	 * Get unique log id
	 *
	 * @return string
	 */
	protected static function getId() : string
	{
		if (!static::$logId) {
			static::$logId = bin2hex(random_bytes(3));
		}

		return static::$logId;
	}
}
