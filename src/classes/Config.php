<?php

namespace HomeServer;

use HomeServer\Traits\SingletonTrait;
use Symfony\Component\Finder\Finder;

class Config
{
	use SingletonTrait;

	/**
	 * @var array
	 */
	protected $data = [];

	public function init() : Config
	{
		$configDirectoryPath = SRC . '/config/';

		if (!file_exists($configDirectoryPath)) {
			throw new \HomeServer\Exception\CoreException('Config directory not found: ' . $configDirectoryPath);
		}

		$finder = new Finder();
		$finder->files()->in($configDirectoryPath);
		foreach ($finder as $file) {
			$path = $file->getRealPath();

			/** @noinspection PhpIncludeInspection */
			$data = require $path;

			if (is_array($data)) {
				$this->data[lcfirst($file->getBasename('.php'))] = $data;
			}
		}

		return $this;
	}

	/**
	 * Get an item from config
	 *
	 * @param string $key
	 * @param mixed  $default
	 *
	 * @return mixed|$default
	 */
	public function get(string $key, $default = null)
	{
		return array_get($this->data, $key, $default);
	}
}
