<?php

use HomeServer\App;

define('ROOT', realpath(__DIR__ . '/../'));
define('SRC', realpath(ROOT . '/src/'));

require_once SRC . '/shortcuts.php';
require_once ROOT . '/vendor/autoload.php';

// App should be always initialized at start
App::instance();
