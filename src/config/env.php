<?php

return [
	'required' => [
		'APP_ENV',
		'APP_KEY',
		'DB_HOST',
		'DB_DATABASE',
		'DB_USERNAME',
		'DB_PASSWORD',
		'GOOGLE_CLIENT_ID',
		'GOOGLE_CLIENT_SECRET'
	]
];
