<?php

use HomeServer\Http\Middleware\AuthMiddleware;

return [
	'aliases' => [
		'auth' => AuthMiddleware::class
	]
];
