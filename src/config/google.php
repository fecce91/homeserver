<?php

return [
	'clientId'     => env('GOOGLE_CLIENT_ID'),
	'clientSecret' => env('GOOGLE_CLIENT_SECRET'),
	'cacheAuth'    => 3600
];
