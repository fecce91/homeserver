<?php

return [
	'cookie'   => [
		'lifetime' => 3600
	],
	'template' => [
		'default' => __DIR__ . '/../../templates/index.html',
		'cache'   => [
			'lifetime' => 1
		]
	]
];
