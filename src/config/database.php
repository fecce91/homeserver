<?php

return [
	'host' => env('DB_HOST'),
	'user' => env('DB_USERNAME'),
	'pass' => env('DB_PASSWORD'),
	'db'   => env('DB_DATABASE'),
	'port' => env('DB_PORT', 3306)
];
