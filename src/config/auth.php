<?php

return [
	'lifetime' => 3600 * 24,
	'cacheSSO' => 60 * 5
];